package pl.azarnow.curator.structures;

import org.apache.commons.lang.SerializationUtils;
import org.apache.curator.framework.CuratorFramework;
import pl.azarnow.curator.CuratorInstance;
import pl.azarnow.curator.CuratorMapUtils;
import pl.azarnow.curator.exceptions.CuratorMapException;
import pl.azarnow.curator.listeners.AbstractCuratorListener;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;

public class CuratorMapImpl<T extends Serializable> extends AbstractCuratorStructure<T> implements CuratorMap<T> {

    public CuratorMapImpl(String mapName, CuratorInstance curatorInstance) throws Exception {
        super(mapName, curatorInstance);
    }

    @Override
    public int size() throws CuratorMapException {
        return mapKeys().size();
    }

    @Override
    public boolean isEmpty() throws CuratorMapException {
        return size() == 0;
    }

    @Override
    public boolean containsKey(Object key) throws CuratorMapException {
        return keySet().contains(key);
    }

    @Override
    public boolean containsValue(Object value) throws CuratorMapException {
        return values().contains(value);
    }

    @Override
    public T get(Object key) throws CuratorMapException {
        String path = getPath(key);

        if (path == null || !checkIfExists(path)) {
            return null;
        }

        try {
            byte[] bytes = curatorInstance.getClient().getData().forPath(path);
            T value = (T) SerializationUtils.deserialize(bytes);
            return value;
        } catch (Exception e) {
            throw new CuratorMapException("Could not get data for " + key, e);
        }
    }

    private String getPath(Object key) {
        if (key instanceof String) {
            return CuratorMapUtils.createPath(name, (String) key);
        } else {
            return null;
        }
    }

    @Override
    public T remove(Object key) throws CuratorMapException {
        if (key == null) {
            return null;
        }

        List<String> keys = mapKeys();
        for (String k : keys) {
            if (key.equals(k)) {
                try {
                    T value = get(k);
                    curatorInstance.getClient().delete().forPath(CuratorMapUtils.createPath(name, k));
                    return value;
                } catch (Exception e) {
                    throw new CuratorMapException("Could not remove " + k, e);
                }
            }
        }

        return null;
    }

    @Override
    public void clear() throws CuratorMapException {
        List<String> keys = mapKeys();
        for (String key : keys) {
            try {
                curatorInstance.getClient().delete().forPath(CuratorMapUtils.createPath(name, key));
            } catch (Exception e) {
                throw new CuratorMapException("Could not remove " + key, e);
            }
        }
    }

    @Override
    public Set<String> keySet() throws CuratorMapException {
        List<String> keys = mapKeys();
        return new HashSet<>(keys);
    }

    @Override
    public Collection<T> values() throws CuratorMapException {
        List<String> keys = mapKeys();
        Collection<T> values = new ArrayList<>(keys.size());
        for (String key : keys) {
            T value = get(key);
            values.add(value);
        }
        return values;

    }

    @Override
    public Set<Entry<String, T>> entrySet() {
        List<String> keys = mapKeys();

        Set<Entry<String, T>> entrySet = new HashSet<>(keys.size());
        for (String key : keys) {
            T value = get(key);
            Entry<String, T> entry = new AbstractMap.SimpleEntry<String, T>(key, value);
            entrySet.add(entry);
        }

        return entrySet;
    }

    @Override
    public void putAll(Map<? extends String, ? extends T> map) {
        for (Entry<? extends String, ? extends T> entry : map.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public T put(String key, T value) throws CuratorMapException {
        try {
            return setOrSave(key, value);
        } catch (Exception e) {
            throw new CuratorMapException("Could not insert [" + key + " - " + value + "] in " + name, e);
        }
    }


    private List<String> mapKeys() throws CuratorMapException {
        try {
            return curatorInstance.getClient().getChildren().forPath(name);
        } catch (Exception e) {
            throw new CuratorMapException("Could not get map keys for map " + name, e);
        }
    }


    @Override
    public void addMapListener(AbstractCuratorListener entryListener) {
        cache.getListenable().addListener(entryListener);
    }

    @Override
    public void removeMapListener(AbstractCuratorListener entryListener) {
        cache.getListenable().removeListener(entryListener);
    }

    @Override
    public void close() throws IOException {
        this.cache.close();
    }
}
