package pl.azarnow.starter.utils;

import org.apache.commons.collections.CollectionUtils;
import pl.azarnow.starter.parameters.Server;
import pl.azarnow.starter.parameters.ZookeeperParams;

public class ConfigContentCreator {

    public static final String createConfigContent(ZookeeperParams params) {
        StringBuilder builder = createStandaloneConfig(params);

        if (!params.isStandalone()) {
            addReplicatedConfig(builder, params);
        }

        return builder.toString();
    }

    private static StringBuilder createStandaloneConfig(ZookeeperParams params) {
        StringBuilder builder = new StringBuilder()
                .append("tickTime=")
                .append(params.getTickTime())
                .append("\ndataDir=")
                .append(params.getDataDir())
                .append("\nclientPort=")
                .append(params.getClientPort());

        return builder;
    }

    private static void addReplicatedConfig(StringBuilder builder, ZookeeperParams params) {
        builder.append("\ninitLimit=")
                .append(params.getInitLimit())
                .append("\nsyncLimit=")
                .append(params.getSyncLimit());

        if (CollectionUtils.isNotEmpty(params.getServers())) {
            for (int i = 0; i < params.getServers().size(); i++) {
                Server server = params.getServers().get(i);

                builder.append("\nserver.")
                        .append(i + 1)
                        .append("=")
                        .append(server.getName())
                        .append(":")
                        .append(server.getPeerPort())
                        .append(":")
                        .append(server.getLeaderPort());
            }
        }
    }

}
