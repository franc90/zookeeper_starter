package pl.azarnow.curator.structures;

import org.apache.commons.lang.SerializationUtils;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.cache.PathChildrenCache;
import org.apache.curator.utils.EnsurePath;
import pl.azarnow.curator.CuratorInstance;
import pl.azarnow.curator.CuratorMapUtils;
import pl.azarnow.curator.exceptions.CuratorMapException;

import java.io.Serializable;

public abstract class AbstractCuratorStructure<T extends Serializable> {

    protected CuratorInstance curatorInstance;
    protected String name;
    protected PathChildrenCache cache;

    public AbstractCuratorStructure(String name, CuratorInstance curatorInstance) throws Exception {
        this.name = "/" + name;
        this.curatorInstance = curatorInstance;

        ensurePath(this.name);

        cache = new PathChildrenCache(curatorInstance.getClient(), this.name, true);
        cache.start();
    }

    private void ensurePath(String path) throws CuratorMapException {
        try {
            EnsurePath ensurePath = new EnsurePath(path);
            ensurePath.ensure(curatorInstance.getClient().getZookeeperClient());
        } catch (Exception e) {
            throw new CuratorMapException("Could not ensure path " + path, e);
        }
    }

    protected boolean checkIfExists(String path) {
        try {
            return curatorInstance.getClient().checkExists().forPath(path) != null;
        } catch (Exception e) {
            throw new CuratorMapException("Error while checking if znode " + path + " exists.", e);
        }
    }

    protected T setOrSave(String nodeName, T value) throws Exception {
        String path = CuratorMapUtils.createPath(name, nodeName);
        byte[] serialized = SerializationUtils.serialize(value);
        CuratorFramework client = curatorInstance.getClient();
        if (checkIfExists(path)) {
            client.setData().forPath(path, serialized);
        } else {
            client.create().creatingParentsIfNeeded().forPath(path, serialized);
        }
        return value;

    }

}
