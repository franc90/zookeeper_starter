package pl.azarnow.starter.parameters;

import java.util.Collections;
import java.util.List;

public class ZookeeperParams {

    private final boolean isStandalone;
    
    private final int myId;

    /**
     * the basic time unit in milliseconds used by ZooKeeper.
     * * It is used to do heartbeats and the minimum session timeout will be twice the tickTime.
     */
    private final int tickTime;

    /**
     * the location to store the in-memory database snapshots and,
     * unless specified otherwise, the transaction log of updates to the database.
     */
    private final String dataDir;

    /**
     * the port to listen for client connections
     */
    private final int clientPort;

    /**
     * timeouts ZooKeeper uses to limit the length of time
     * the ZooKeeper servers in quorum have to connect to a leader
     */
    private final int initLimit;

    /**
     * limits how far out of date a server can be from a leader
     */
    private final int syncLimit;

    /**
     * servers that make up the ZooKeeper service
     */
    private final List<Server> servers;

    private ZookeeperParams(boolean isStandalone, int myId, int tickTime, String dataDir, int clientPort, int initLimit, int syncLimit, List<Server> servers) {
        this.isStandalone = isStandalone;
        this.myId = myId;
        this.tickTime = tickTime;
        this.dataDir = dataDir;
        this.clientPort = clientPort;
        this.initLimit = initLimit;
        this.syncLimit = syncLimit;
        this.servers = servers;
    }

    public boolean isStandalone() {
        return isStandalone;
    }

    public int getMyId() {
        return myId;
    }

    public int getTickTime() {
        return tickTime;
    }

    public String getDataDir() {
        return dataDir;
    }

    public int getClientPort() {
        return clientPort;
    }

    public int getInitLimit() {
        return initLimit;
    }

    public int getSyncLimit() {
        return syncLimit;
    }

    public List<Server> getServers() {
        return servers;
    }

    @Override
    public String toString() {
        return "ZookeeperParams{" +
                "isReplicated=" + isStandalone +
                ", tickTime=" + tickTime +
                ", dataDir='" + dataDir + '\'' +
                ", clientPort=" + clientPort +
                ", initLimit=" + initLimit +
                ", syncLimit=" + syncLimit +
                ", servers=" + servers +
                '}';
    }

    public static class ZookeeperParamsBuilder {
        private boolean isStandalone = true;
        private int myId = 1;
        private int tickTime = 2000;
        private String dataDir = "/var/lib/zookeeper";
        private int clientPort = 2181;
        private int initLimit = 5;
        private int syncLimit = 2;
        private List<Server> servers = Collections.emptyList();

        public ZookeeperParams build() {
            return new ZookeeperParams(isStandalone, myId, tickTime, dataDir, clientPort, initLimit, syncLimit, servers);
        }

        public ZookeeperParamsBuilder standalone(boolean isStandalone) {
            this.isStandalone = isStandalone;
            return this;
        }
        
        public ZookeeperParamsBuilder myId(int myId) {
            this.myId = myId;
            return this;
        }

        public ZookeeperParamsBuilder clientPort(int clientPort) {
            this.clientPort = clientPort;
            return this;
        }

        public ZookeeperParamsBuilder dataDir(String dataDir) {
            this.dataDir = dataDir;
            return this;
        }

        public ZookeeperParamsBuilder initLimit(int initLimit) {
            this.initLimit = initLimit;
            return this;
        }


        public ZookeeperParamsBuilder syncLimit(int syncLimit) {
            this.syncLimit = syncLimit;
            return this;
        }

        public ZookeeperParamsBuilder servers(List<Server> servers) {
            this.servers = servers;
            return this;
        }
    }
}
