package pl.azarnow.starter.utils;

public class OSChecker {

    private static String OS = System.getProperty("os.name").toLowerCase();

    public static boolean isWindows() {
        return OS.indexOf("win") >= 0;
    }
    
}
