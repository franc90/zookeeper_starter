package pl.azarnow.curator;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;

public class CuratorInstance implements AutoCloseable {

    private CuratorFramework client;

    private String zookeeperConnectionString;

    public CuratorInstance(String serverHost, String serverPort) {
        this(serverHost + ":" + serverPort);
    }

    public CuratorInstance(String zookeeperConnectionString) {
        this.zookeeperConnectionString = zookeeperConnectionString;
    }

    public CuratorFramework getClient() {

        if (client == null) {
            createInstance();
        }
        return client;
    }

    private void createInstance() {
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);
        client = CuratorFrameworkFactory.newClient(zookeeperConnectionString, retryPolicy);
        client.start();
    }

    @Override
    public void close() throws Exception {
        if (client != null) {
            client.close();
            client = null;
        }
    }
}
