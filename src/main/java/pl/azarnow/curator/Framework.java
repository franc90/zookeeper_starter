package pl.azarnow.curator;

import org.apache.commons.lang.SerializationUtils;
import org.apache.curator.framework.recipes.cache.ChildData;
import org.apache.curator.test.TestingServer;
import pl.azarnow.curator.listeners.AbstractCuratorListener;
import pl.azarnow.curator.structures.CuratorMap;
import pl.azarnow.curator.structures.CuratorMapImpl;

public class Framework {

    public static void main(String[] args) throws Exception {
        TestingServer server = new TestingServer();
        String connectString = server.getConnectString();

        System.out.println(connectString);

        try (CuratorInstance instance = new CuratorInstance(connectString)) {
            try (CuratorInstance curatorInstance = new CuratorInstance(connectString)) {
                System.out.println("Creating map");

                CuratorMap<String> curatorMap = new CuratorMapImpl<>("jasiek", instance);

                CuratorMap<String> curatorMap2 = new CuratorMapImpl<>("jasiek", curatorInstance);
                curatorMap2.addMapListener(new AbstractCuratorListener() {
                    @Override
                    protected void entryAdded(ChildData childData) {
                        System.out.println("Added " + childData.getPath() + " " + SerializationUtils.deserialize(childData.getData()));
                    }

                    @Override
                    protected void entryRemoved(ChildData childData) {
                        System.out.println("Remove " + childData.getPath() + " " + SerializationUtils.deserialize(childData.getData()));
                    }

                    @Override
                    protected void entryUpdated(ChildData childData) {
                        System.out.println("Updated " + childData.getPath() + " " + SerializationUtils.deserialize(childData.getData()));
                    }
                });

                curatorMap2.put("key", "val");
                curatorMap.put("key2", "val");
                curatorMap2.put("key", "jas");
                curatorMap.remove("key2");


            }
        }

        server.close();
    }
}
