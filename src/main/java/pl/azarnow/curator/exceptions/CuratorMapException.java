package pl.azarnow.curator.exceptions;

public class CuratorMapException extends RuntimeException {

    public CuratorMapException(String s, Throwable throwable) {
        super(s, throwable);
    }

}
