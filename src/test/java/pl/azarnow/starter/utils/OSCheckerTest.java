package pl.azarnow.starter.utils;

import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.*;

public class OSCheckerTest {

    @Test
    public void checkWindowsTrue() throws NoSuchFieldException, IllegalAccessException {
        Field os = OSChecker.class.getDeclaredField("OS");
        os.setAccessible(true);
        os.set(null, "win");

        boolean isWindows = OSChecker.isWindows();
        assertTrue(isWindows);
        
    }
    
    @Test
    public void checkWindowsFalse() throws NoSuchFieldException, IllegalAccessException {
        Field os = OSChecker.class.getDeclaredField("OS");
        os.setAccessible(true);
        os.set(null, "linux");

        boolean isWindows = OSChecker.isWindows();
        assertFalse(isWindows);
    }
    
}