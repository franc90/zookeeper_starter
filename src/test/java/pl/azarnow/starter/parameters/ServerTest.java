package pl.azarnow.starter.parameters;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class ServerTest {
    
    @Test
    public void noConfig() {
        Server server = new Server.ServerBuilder().build();
        
        assertEquals(server.getName(), "localhost");
        assertEquals(server.getPeerPort(), 2888);
        assertEquals(server.getLeaderPort(), 3888);
    }
    
    @Test
    public void withConfig() {
        Server server = new Server.ServerBuilder()
                .name("192.168.0.1")
                .peerPort(1234)
                .leaderPort(4321)
                .build();
        
        assertEquals(server.getName(), "192.168.0.1");
        assertEquals(server.getLeaderPort(), 4321);
        assertEquals(server.getPeerPort(), 1234);
    }
}
