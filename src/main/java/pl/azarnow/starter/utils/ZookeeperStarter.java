package pl.azarnow.starter.utils;

import pl.azarnow.starter.parameters.ZookeeperParams;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Logger;

public class ZookeeperStarter {

    public static final Logger log = Logger.getLogger(ZookeeperStarter.class.getName());

    public static final String ZOOKEEPER_FOLDER_NAME = "zookeeper-3.4.6";

    private static final String CONFIG_FILE = "zoo.cfg";

    private static final String MYID_FILE = "myid";

    private static final String SERVER_FILE_WINDOWS = "zkServer.cmd";

    private static final String SERVER_FILE_UNIX = "zkServer.sh";

    public void start(ZookeeperParams params) throws IOException {
        createMyIdFile(params);
        
        String config = ConfigContentCreator.createConfigContent(params);
        createConfigFile(config);
        
        startServer();
    }

    private void createMyIdFile(ZookeeperParams params) throws IOException {
        if (params.isStandalone()) {
            return;
        }

        Files.write(Paths.get(params.getDataDir(), MYID_FILE), String.valueOf(params.getMyId()).getBytes());
    }

    private void createConfigFile(String config) throws IOException {
        Files.write(Paths.get(ZOOKEEPER_FOLDER_NAME, "conf", CONFIG_FILE), config.getBytes());
    }

    private void startServer() throws IOException {
        if (OSChecker.isWindows()) {
            String path = Paths.get(ZOOKEEPER_FOLDER_NAME, "bin", SERVER_FILE_WINDOWS).toString();
            runScript("cmd.exe", "/C", "start", path);
        } else {
            String path = Paths.get(ZOOKEEPER_FOLDER_NAME, "bin", SERVER_FILE_UNIX).toString();
            runScript(path, "start");
        }
    }
    
    private void runScript(String... processParams) throws IOException {
        ProcessBuilder processBuilder = new ProcessBuilder(processParams);
        processBuilder.start();
    }

    public void stop() throws IOException {
        if (OSChecker.isWindows()) {
            System.err.println("Kill it manually.");
        } else {
            String path = Paths.get(ZOOKEEPER_FOLDER_NAME, "bin", SERVER_FILE_UNIX).toString();
            runScript(path, "stop");
        }
    }


}
