package pl.azarnow.starter;


import pl.azarnow.starter.parameters.Server;
import pl.azarnow.starter.parameters.ZookeeperParams;
import pl.azarnow.starter.utils.ZookeeperStarter;

import java.io.IOException;
import java.util.Arrays;

public class App {

    public static void main(String[] args) throws InterruptedException, IOException {
        Server.ServerBuilder builder = new Server.ServerBuilder();
        ZookeeperParams params = new ZookeeperParams.ZookeeperParamsBuilder()
                .standalone(false)
                .dataDir("data")
                .servers(Arrays.asList(builder.name("localhost").build()))
                .build();
        
        ZookeeperStarter starter = new ZookeeperStarter();
        starter.start(params);

        Thread.sleep(15000);

        starter.stop();
    }
    
}
