package pl.azarnow.starter.utils;

import org.junit.Test;
import pl.azarnow.starter.parameters.Server;
import pl.azarnow.starter.parameters.ZookeeperParams;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ConfigContentCreatorTest {

    @Test
    public void standaloneConfig() throws IOException, URISyntaxException {
        ZookeeperParams params = new ZookeeperParams.ZookeeperParamsBuilder().build();

        String configFile = ConfigContentCreator.createConfigContent(params).replaceAll("\n", "");
        String filePath = "configFiles" + File.separator + "standalone.cfg";
        String file = getFile(filePath);

        assertEquals(file, configFile);
    }

    @Test
    public void replicatedConfig() throws IOException, URISyntaxException {
        Server.ServerBuilder builder = new Server.ServerBuilder();
        List<Server> servers = new ArrayList<>();
        servers.add(builder.name("zoo1").build());
        servers.add(builder.name("zoo2").build());
        servers.add(builder.name("zoo3").build());

        ZookeeperParams params = new ZookeeperParams.ZookeeperParamsBuilder().
                standalone(false)
                .servers(servers)
                .build();

        String configFile = ConfigContentCreator.createConfigContent(params).replaceAll("\n", "");
        String filePath = "configFiles" + File.separator + "replicated.cfg";
        String file = getFile(filePath);

        assertEquals(file, configFile);
    }

    private String getFile(String path) throws URISyntaxException, IOException {
        URL resource = getClass().getClassLoader().getResource(path);
        assertNotNull(resource);
        String file = Files.readAllLines(Paths.get(resource.toURI())).stream().reduce("", (x, y) -> x + y);

        return file.replaceAll("\n", "");
    }

}