package pl.azarnow.starter.parameters;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ZookeeperParamsTest {

    @Test
    public void noConfig() {
        ZookeeperParams params = new ZookeeperParams.ZookeeperParamsBuilder().build();
        
        assertNotNull(params);
        assertFalse(params.isStandalone());
        assertEquals(params.getMyId(), 1);
        assertEquals(params.getClientPort(), 2181);
        assertEquals(params.getDataDir(), "/var/lib/zookeeper");
        assertEquals(params.getInitLimit(), 5);
        assertEquals(params.getSyncLimit(), 2);
        assertNotNull(params.getServers());
        assertEquals(params.getServers().size(), 0);
    }
    
    @Test
    public void withConfig() {
        Server.ServerBuilder builder = new Server.ServerBuilder();
        List<Server> servers = new ArrayList<Server>();
        servers.add(builder.build());
        servers.add(builder.name("jas").build());
        
        ZookeeperParams params = new ZookeeperParams.ZookeeperParamsBuilder()
                .standalone(false)
                .myId(2)
                .clientPort(2222)
                .dataDir("/var/lib/myFolder")
                .initLimit(3)
                .syncLimit(11)
                .servers(servers)
                .build();
        
        assertNotNull(params);
        assertTrue(params.isStandalone());
        assertEquals(params.getMyId(), 2);
        assertEquals(params.getClientPort(), 2222);
        assertEquals(params.getDataDir(), "/var/lib/myFolder");
        assertEquals(params.getInitLimit(), 3);
        assertEquals(params.getSyncLimit(), 11);
        assertNotNull(params.getServers());
        assertEquals(params.getServers().size(), 2);
        assertEquals(params.getServers().get(1).getName(), "jas");
    }
}