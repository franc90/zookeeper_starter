package pl.azarnow.curator.structures;

import pl.azarnow.curator.listeners.AbstractCuratorListener;

import java.io.Closeable;
import java.util.Map;

public interface CuratorMap<T> extends Map<String, T>, Closeable {

    void addMapListener(AbstractCuratorListener entryListener);

    void removeMapListener(AbstractCuratorListener entryListener);


}
