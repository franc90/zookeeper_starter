package pl.azarnow.curator.structures;

import org.apache.commons.lang.SerializationUtils;
import org.apache.curator.framework.recipes.cache.ChildData;
import org.apache.curator.test.TestingServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pl.azarnow.curator.CuratorInstance;
import pl.azarnow.curator.listeners.AbstractCuratorListener;

import java.util.*;

import static org.junit.Assert.*;

public class CuratorMapTest {

    private TestingServer server;

    private CuratorInstance curatorInstance;

    private CuratorMapImpl<String> curatorMap;

    private static final String KEY = "KEY";

    private static final String VALUE = "VALUE";


    @Before
    public void init() throws Exception {
        server = new TestingServer();
        curatorInstance = new CuratorInstance(server.getConnectString());
        curatorMap = new CuratorMapImpl<>("testMap", curatorInstance);
    }

    @After
    public void cleanUp() throws Exception {
        curatorInstance.close();
        server.close();

    }

    @Test
    public void testSize() throws Exception {
        assertEquals(curatorMap.size(), 0);

        curatorMap.put(KEY, VALUE);
        assertEquals(curatorMap.size(), 1);

        curatorMap.remove(KEY);
        assertEquals(curatorMap.size(), 0);
    }

    @Test
    public void testIsEmpty() throws Exception {
        assertTrue(curatorMap.isEmpty());

        curatorMap.put(KEY, VALUE);
        assertFalse(curatorMap.isEmpty());
    }

    @Test
    public void testContainsKey() throws Exception {
        assertFalse(curatorMap.containsKey(KEY));

        curatorMap.put(KEY, VALUE);
        assertTrue(curatorMap.containsKey(KEY));

        curatorMap.remove(KEY);
        assertFalse(curatorMap.containsKey(KEY));
    }

    @Test
    public void testContainsValue() throws Exception {
        assertFalse(curatorMap.containsValue(VALUE));

        curatorMap.put(KEY, "randomValue");
        assertFalse(curatorMap.containsValue(VALUE));

        curatorMap.put(KEY, VALUE);
        assertTrue(curatorMap.containsValue(VALUE));

        curatorMap.remove(KEY);
        assertFalse(curatorMap.containsValue(VALUE));
    }

    @Test
    public void testGet() throws Exception {
        assertNull(curatorMap.get(KEY));

        curatorMap.put(KEY, VALUE);
        assertEquals(curatorMap.get(KEY), VALUE);
    }

    @Test
    public void testRemove() throws Exception {
        assertNull(curatorMap.remove(KEY));

        curatorMap.put(KEY, VALUE);
        assertNull(curatorMap.remove("some key"));

        assertEquals(curatorMap.remove(KEY), VALUE);
    }

    @Test
    public void testClear() throws Exception {
        curatorMap.put(KEY + 1, VALUE + 1);
        curatorMap.put(KEY + 2, VALUE + 2);
        curatorMap.put(KEY + 3, VALUE + 3);

        assertFalse(curatorMap.isEmpty());

        curatorMap.clear();
        assertTrue(curatorMap.isEmpty());
    }

    @Test
    public void testKeySet() throws Exception {
        Set<String> keySet = curatorMap.keySet();
        assertNotNull(keySet);
        assertEquals(keySet.size(), 0);

        curatorMap.put(KEY, VALUE);
        curatorMap.put(KEY + 1, VALUE + 1);

        keySet = curatorMap.keySet();
        assertNotNull(keySet);
        assertEquals(keySet.size(), 2);
        assertTrue(keySet.contains(KEY));

        curatorMap.remove(KEY);
        keySet = curatorMap.keySet();
        assertFalse(keySet.contains(KEY));
    }

    @Test
    public void testValues() throws Exception {
        Collection<String> values = curatorMap.values();
        assertNotNull(values);
        assertEquals(values.size(), 0);

        curatorMap.put(KEY, VALUE);
        curatorMap.put(KEY + 1, VALUE + 1);

        values = curatorMap.values();
        assertNotNull(values);
        assertEquals(values.size(), 2);
        assertTrue(values.contains(VALUE));

        curatorMap.remove(KEY);
        values = curatorMap.values();
        assertFalse(values.contains(VALUE));
    }

    @Test
    public void testEntrySet() throws Exception {
        Map.Entry<String, String> entry = new AbstractMap.SimpleEntry<>(KEY, VALUE);
        Set<Map.Entry<String, String>> entries = curatorMap.entrySet();
        assertNotNull(entries);
        assertTrue(entries.isEmpty());

        curatorMap.put(KEY, VALUE);
        curatorMap.put(KEY + 1, VALUE + 1);

        entries = curatorMap.entrySet();
        assertEquals(entries.size(), 2);
        assertTrue(entries.contains(entry));

        curatorMap.remove(KEY);
        entries = curatorMap.entrySet();
        assertEquals(entries.size(), 1);
        assertFalse(entries.contains(entry));
    }

    @Test
    public void testPutAll() throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put(KEY, VALUE);
        map.put(KEY + 1, VALUE + 1);
        map.put(KEY + 2, VALUE + 2);

        assertTrue(curatorMap.isEmpty());
        curatorMap.putAll(map);

        assertFalse(curatorMap.isEmpty());
        assertEquals(curatorMap.get(KEY), VALUE);
    }

    @Test
    public void testPut() throws Exception {
        assertNull(curatorMap.get(KEY));

        assertEquals(curatorMap.put(KEY, VALUE), VALUE);
        assertEquals(curatorMap.size(), 1);
        assertEquals(curatorMap.get(KEY), VALUE);
    }

    @Test
    public void testMultipleCurators() throws Exception {
        CuratorInstance instance = new CuratorInstance(server.getConnectString());
        Map<String, String> curatorMap2 = new CuratorMapImpl<>("testMap", instance);

        curatorMap.put(KEY, VALUE);
        assertNotNull(curatorMap2.get(KEY));
        assertEquals(curatorMap2.get(KEY), VALUE);

        curatorMap2.remove(KEY);
        assertNull(curatorMap.get(KEY));

        instance.close();
    }

    @Test
    public void testListeners() throws Exception {
        CuratorInstance instance = new CuratorInstance(server.getConnectString());
        CuratorMap<String> curatorMap2 = new CuratorMapImpl<>("testMap", instance);

        curatorMap2.addMapListener(new AbstractCuratorListener() {
            @Override
            protected void entryAdded(ChildData childData) {
                String deserialized = (String) SerializationUtils.deserialize(childData.getData());
                assertNotNull(deserialized);
                assertEquals(deserialized, VALUE);
            }

            @Override
            protected void entryRemoved(ChildData childData) {
                String deserialized = (String) SerializationUtils.deserialize(childData.getData());
                assertNotNull(deserialized);
                assertEquals(deserialized, KEY);
            }

            @Override
            protected void entryUpdated(ChildData childData) {
                String deserialized = (String) SerializationUtils.deserialize(childData.getData());
                assertNotNull(deserialized);
                assertEquals(deserialized, KEY);
            }
        });

        curatorMap.put(KEY, VALUE);
        curatorMap.put(KEY, KEY);
        curatorMap.put(KEY, KEY);
        curatorMap.remove(KEY);
        
        instance.close();
    }
}
