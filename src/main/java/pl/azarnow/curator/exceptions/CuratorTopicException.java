package pl.azarnow.curator.exceptions;

public class CuratorTopicException extends RuntimeException {

    public CuratorTopicException(String s, Throwable throwable) {
        super(s, throwable);
    }

}
