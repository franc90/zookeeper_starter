package pl.azarnow.starter.parameters;

/**
 * If you want to test multiple servers on a single machine, specify the servername as localhost with
 * unique quorum & leader election ports (i.e. 2888:3888, 2889:3889, 2890:3890 in the example above)
 * for each server.X in that server's config file.
 * Of course separate dataDirs and distinct clientPorts are also necessary
 * (in the above replicated example, running on a single localhost, you would still have three config files)
 */
public class Server {

    /**
     * ip/hostname of server
     */
    private final String name;

    /**
     * port for agreeing on updates, talks follower<-->leader
     */
    private final int peerPort;

    /**
     * port for leader election
     */
    private final int leaderPort;

    private Server(String name, int peerPort, int leaderPort) {
        this.name = name;
        this.peerPort = peerPort;
        this.leaderPort = leaderPort;
    }

    public String getName() {
        return name;
    }

    public int getPeerPort() {
        return peerPort;
    }

    public int getLeaderPort() {
        return leaderPort;
    }


    @Override
    public String toString() {
        return "Server{" +
                "name='" + name + '\'' +
                ", peerPort=" + peerPort +
                ", leaderPort=" + leaderPort +
                '}';
    }

    public static class ServerBuilder {
        private String name = "localhost";
        private int peerPort = 2888;
        private int leaderPort = 3888;

        public ServerBuilder name(String name) {
            this.name = name;
            return this;
        }

        public ServerBuilder peerPort(int peerPort) {
            this.peerPort = peerPort;
            return this;
        }

        public ServerBuilder leaderPort(int leaderPort) {
            this.leaderPort = leaderPort;
            return this;
        }

        public Server build() {
            return new Server(name, peerPort, leaderPort);
        }
    }
}
