package pl.azarnow.curator;

import org.apache.commons.collections.CollectionUtils;

import java.util.*;

public class CuratorMapUtils {

    private static final String PATH_SEPARATOR = "/";

    public static final String EMPTY_STRING = "";

    public static String getKey(String keyPath) {
        if (keyPath == null || !keyPath.contains(PATH_SEPARATOR)) {
            return EMPTY_STRING;
        }

        return keyPath.substring(keyPath.lastIndexOf(PATH_SEPARATOR));
    }

    public static Set<String> getKeys(Collection<String> keyPaths) {
        if (CollectionUtils.isEmpty(keyPaths)) {
            return Collections.emptySet();
        }

        Set<String> keys = new HashSet<>(keyPaths.size());
        for (String keyPath : keyPaths) {
            String key = getKey(keyPath);
            keys.add(key);
        }

        return keys;
    }

    public static String createPath(String... pathVals) {
        StringJoiner joiner = new StringJoiner(PATH_SEPARATOR);

        for (String val : pathVals) {
            joiner.add(val);
        }

        return joiner.toString();
    }

    public static Set<String> createPath(Set<? extends String> keySet, String... pathVals) {
        if (CollectionUtils.isEmpty(keySet)) {
            return Collections.emptySet();
        }

        StringJoiner joiner = new StringJoiner(PATH_SEPARATOR);
        for (String val : pathVals) {
            joiner.add(val);
        }
        String prefix = joiner.toString();

        Set<String> paths = new HashSet<>(keySet.size());
        for (String key : keySet) {
            String path = createPath(prefix, key);
            paths.add(path);
        }

        return paths;
    }

}
