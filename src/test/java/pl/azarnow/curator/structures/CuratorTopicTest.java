package pl.azarnow.curator.structures;

import org.apache.commons.lang.SerializationUtils;
import org.apache.curator.framework.recipes.cache.ChildData;
import org.apache.curator.test.TestingServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pl.azarnow.curator.CuratorInstance;
import pl.azarnow.curator.listeners.AbstractCuratorListener;

import java.io.IOException;

import static org.junit.Assert.*;

public class CuratorTopicTest {
    
    private TestingServer server;

    private CuratorInstance curatorInstance;

    private CuratorInstance curatorInstance2;

    @Before
    public void init() throws Exception {
        server = new TestingServer();
        curatorInstance = new CuratorInstance(server.getConnectString());
        curatorInstance2 = new CuratorInstance(server.getConnectString());
    }
    
    @After
    public void cleanUp() throws Exception {
        curatorInstance.close();
        curatorInstance2.close();
        server.close();
    }
    
    @Test
    public void testCommunication() throws Exception {
        String topicName = "topic";
        String msg1 = "msg1";
        String msg2 = "msg2";
        
        CuratorTopic<String> topic1 = new CuratorTopicImpl<>(topicName, curatorInstance);
        CuratorTopic<String> topic2 = new CuratorTopicImpl<>(topicName, curatorInstance2);
        
        topic1.addListener(new AbstractCuratorListener() {
            @Override
            protected void entryAdded(ChildData childData) {
                String deserialize = (String) SerializationUtils.deserialize(childData.getData());
                assertNotNull(deserialize);
                assertEquals(deserialize, msg1);
                System.out.println(deserialize);
            }

            @Override
            protected void entryRemoved(ChildData childData) {
                assertTrue(false);
            }

            @Override
            protected void entryUpdated(ChildData childData) {
                String deserialize = (String) SerializationUtils.deserialize(childData.getData());
                assertNotNull(deserialize);
                assertEquals(deserialize, msg2);
                System.out.println(deserialize);
            }
        });
        
        topic2.publish(msg1);
        topic2.publish(msg2);
        topic2.publish(msg2);
    }

}