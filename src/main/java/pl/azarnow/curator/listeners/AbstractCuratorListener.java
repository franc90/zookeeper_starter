package pl.azarnow.curator.listeners;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.cache.ChildData;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheListener;

public abstract class AbstractCuratorListener implements PathChildrenCacheListener {

    protected abstract void entryAdded(ChildData childData);

    protected abstract void entryRemoved(ChildData childData);

    protected abstract void entryUpdated(ChildData childData);

    @Override
    public void childEvent(CuratorFramework client, PathChildrenCacheEvent event) throws Exception {
        switch (event.getType()) {
            case CHILD_ADDED:
                entryAdded(event.getData());
                break;
            case CHILD_REMOVED:
                entryRemoved(event.getData());
                break;
            case CHILD_UPDATED:
                entryUpdated(event.getData());
                break;
            default:
                break;
        }
    }
}
