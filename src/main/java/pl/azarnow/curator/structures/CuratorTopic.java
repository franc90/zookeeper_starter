package pl.azarnow.curator.structures;

import pl.azarnow.curator.listeners.AbstractCuratorListener;

public interface CuratorTopic<T> {

    void publish(T message);

    void addListener(AbstractCuratorListener curatorListener);

    void removeListener(AbstractCuratorListener curatorListener);

}
