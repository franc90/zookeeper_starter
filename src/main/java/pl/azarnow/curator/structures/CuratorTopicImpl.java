package pl.azarnow.curator.structures;

import pl.azarnow.curator.CuratorInstance;
import pl.azarnow.curator.exceptions.CuratorTopicException;
import pl.azarnow.curator.listeners.AbstractCuratorListener;

import java.io.Serializable;

public class CuratorTopicImpl<T extends Serializable> extends AbstractCuratorStructure<T> implements CuratorTopic<T> {

    private static final String TOPIC = "topic";

    public CuratorTopicImpl(String topicName, CuratorInstance curatorInstance) throws Exception {
        super(topicName, curatorInstance);
    }

    @Override
    public void publish(T message) throws CuratorTopicException {
        try {
            setOrSave(TOPIC, message);
        } catch (Exception e) {
            throw new CuratorTopicException("Could not publish message " + message, e);
        }
    }

    @Override
    public void addListener(AbstractCuratorListener curatorListener) {
        cache.getListenable().addListener(curatorListener);
    }

    @Override
    public void removeListener(AbstractCuratorListener curatorListener) {
        cache.getListenable().removeListener(curatorListener);
    }
}
